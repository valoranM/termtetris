#ifndef GRIDE_H
#define GRIDE_H

typedef enum Color
{
    NONE,
    ORANGE,
    BLUE,
    MAGENTA,
    YELLOW,
    CYAN,
    RED,
    GREEN,
    OUT
} color;

extern void set_piece(int *y_check);

extern int can_go_down();

extern int can_go_right();

extern int can_go_left();

extern int can_rotate();

color get_case(int x, int y);

void set_case(int val, int x, int y);

extern int loose();

#endif /* GRIDE_H */