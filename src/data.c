#include <stdlib.h>
#include <stdio.h>

static int tetris, next_tetris;
static int rotate;
static long line, score;
static int level, line_next;

static int x, y;

static void add_score(int l);
static void add_level();
extern void update_tetris();

int init()
{
    line = 0;
    score = 0;
    level = 0;
    line_next = 10;
    update_tetris();
    update_tetris();
}

int get_tetris()
{
    return tetris;
}

int get_next_tetris()
{
    return next_tetris;
}

int get_rotate()
{
    return rotate;
}

int get_x()
{
    return x;
}

int get_y()
{
    return y;
}

int get_level()
{
    return level;
}

long get_score()
{
    return score;
}

long get_line()
{
    return line;
}

void rotate_piece()
{
    rotate = (rotate + 1) % 4;
}

void move_left()
{
    x--;
}

void move_right()
{
    x++;
}

void move_down()
{
    y++;
}

void update_tetris()
{
    x = 3;
    rotate = 0;
    tetris = next_tetris;
    next_tetris = rand() % 7;
    switch (tetris)
    {
    case 0:
    case 1:
    case 2:
    case 5:
    case 6:
        y = -1;
        break;

    case 3:
        y = 0;
        break;

    case 4:
        y = -2;

    default:
        break;
    }
}

void add_line(int l)
{
    line += l;
    add_score(l);
    if (line_next - l <= 0)
    {
        line_next += (100 < level * 10) ? 100 : level * 10;
        level++;
    }
    line_next - l;
}

int get_speed()
{
    int speed;
    if (level < 8)
    {
        speed = 48 - (5 * level);
    }
    else if (level == 9)
    {
        speed = 6;
    }
    else if (level <= 12)
    {
        speed = 5;
    }
    else if (level <= 15)
    {
        speed = 4;
    }
    else if (level <= 18)
    {
        speed = 3;
    }
    else if (level <= 28)
    {
        speed = 2;
    }
    else
    {
        speed = 1;
    }
    return speed;
}

static void add_score(int l)
{
    switch (l)
    {
    case 1:
        score += 40 * (level + 1);
        break;

    case 2:
        score += 100 * (level + 1);
        break;

    case 3:
        score += 300 * (level + 1);
        break;

    case 4:
        score += 1200 * (level + 1);
        break;

    default:
        break;
    }
}