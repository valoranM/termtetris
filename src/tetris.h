#ifndef TETRIS_H
#define TETRIS_H

extern void init_tetris();

extern void start_tetris();

extern void stop_tetris();

#endif /* TETRIS_H */