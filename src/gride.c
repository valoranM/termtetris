#include <stdlib.h>

#include "data.h"
#include "gride.h"

#define WIDTH 10
#define HEIGHT 20

static color gride[WIDTH * HEIGHT] = {0};

void set_piece(int *y_check)
{
    int tet = get_tetris(),
        rot = get_rotate(),
        y = get_y(),
        x = get_x(),
        tab = -1;
    for (size_t i = 0; i < 4; i++)
    {
        for (size_t j = 0; j < 4; j++)
        {
            if (tetris[tet][rot][i][j])
            {
                set_case(tet + 1, x + j, y + i);
                if (tab == 0 || y_check[tab] != i + y)
                {
                    tab++;
                    y_check[tab] = i + y;
                }
            }
        }
    }
}

int can_go_down()
{
    int tet = get_tetris(),
        rot = get_rotate();
    int v_x, v_y;
    for (size_t i = 0; i < 4; i++)
    {
        for (size_t j = 0; j < 4; j++)
        {
            if (tetris[get_tetris()][get_rotate()][i][j])
            {
                v_x = get_x() + j;
                v_y = get_y() + i + 1;
                if (tetris[tet][rot][i][j] &&
                    (v_y >= HEIGHT || get_case(v_x, v_y)))
                {
                    return 0;
                }
            }
        }
    }
    return 1;
}

int can_go_right()
{
    int tet = get_tetris(),
        rot = get_rotate();
    int v_x, v_y;
    for (size_t i = 0; i < 4; i++)
    {
        for (size_t j = 0; j < 4; j++)
        {
            if (tetris[tet][rot][i][j])
            {
                v_x = get_x() + j + 1;
                v_y = get_y() + i;
                if (tetris[tet][rot][i][j] &&
                    (v_x >= WIDTH || get_case(v_x, v_y)))
                {
                    return 0;
                }
            }
        }
    }
    return 1;
}

int can_go_left()
{
    int tet = get_tetris(),
        rot = get_rotate();
    int v_x, v_y;
    for (size_t i = 0; i < 4; i++)
    {
        for (size_t j = 0; j < 4; j++)
        {
            if (tetris[tet][rot][i][j])
            {
                v_x = get_x() + j - 1;
                v_y = get_y() + i;
                if (tetris[tet][rot][i][j] &&
                    (v_x < 0 || get_case(v_x, v_y)))
                {
                    return 0;
                }
            }
        }
    }
    return 1;
}

int can_rotate()
{
    int tet = get_tetris(),
        rot = get_rotate();
    int v_x, v_y;
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            if (tetris[tet][rot + 1][i][j])
            {
                v_x = get_x() + j;
                v_y = get_y() + i;
                if (v_x < 0 || v_x >= WIDTH || v_y >= HEIGHT ||
                    get_case(v_x, v_y))
                {

                    return 0;
                }
            }
        }
    }
    return 1;
}

color get_case(int x, int y)
{
    if (x >= 0 && x < WIDTH && y >= 0 && y < HEIGHT)
    {
        return gride[x + y * WIDTH];
    }
    else
    {
        return 0;
    }
}

void set_case(int val, int x, int y)
{
    gride[x + y * WIDTH] = val;
}

int loose()
{
    int tet = get_tetris(),
        rot = get_rotate(),
        y = get_y(),
        x = get_x();
    for (size_t i = 0; i < 4; i++)
    {
        for (size_t j = 0; j < 4; j++)
        {
            if (tetris[tet][rot][i][j])
            {
                if(y + i >= 0 && x + j >= 0 && get_case(x + j, y + i))
                {
                    return 1;
                }
            }
        }
    }
    return 0;
}