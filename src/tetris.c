#include <ncurses.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "gride.h"
#include "data.h"

static int box_width, x_begin;
static int box_height, y_begin;
static int virtualScale;
static int speed;

static WINDOW *win,
    *next,
    *score;

int box_next_x, box_next_y;

static time_t last_frame;

static void draw_case_absolute(int x, int y, color c, int x_vect, int y_vect);
static int input(int code);
static void draw_case(int x, int y, color c);
static void draw_piece();
static void erase_piece();
static int set_piece_tetris();
static void erase_line(int y);
static void teleport_down_cols(int x, int y);
static void draw_case_next(int x, int y, color c);
static void draw_next_piece();
static void draw_score();

void init_tetris()
{
    initscr();

    // to make getch() not blocking
    nodelay(stdscr, TRUE);

    // to disable cursor
    curs_set(0);

    // to fix arrow keys detection
    intrflush(stdscr, FALSE);
    keypad(stdscr, TRUE);

    // to disable the keystroke display
    noecho();

    use_default_colors();
    start_color();

    init_pair(NONE, -1, -1);
    init_pair(ORANGE, -1, COLOR_GREEN);
    init_pair(BLUE, -1, COLOR_BLUE);
    init_pair(MAGENTA, -1, COLOR_MAGENTA);
    init_pair(YELLOW, -1, COLOR_YELLOW);
    init_pair(CYAN, -1, COLOR_CYAN);
    init_pair(RED, -1, COLOR_RED);
    init_pair(GREEN, -1, COLOR_GREEN);

    virtualScale = (LINES - 2) / 20;
    box_height = virtualScale * 20 + 2;
    box_width = virtualScale * 10 * 2 + 2;
    x_begin = (COLS - box_width) / 2;
    y_begin = LINES - box_height;

    win = subwin(stdscr, box_width, box_height,
                 y_begin, x_begin);
    box(win, ACS_VLINE, ACS_HLINE);

    box_next_y = y_begin;
    box_next_x = x_begin + 11 * (virtualScale * 2);
    next = subwin(stdscr, 2 * virtualScale + 2, 4 * (virtualScale * 2) + 2,
                  box_next_y, box_next_x);
    box(next, ACS_VLINE, ACS_HLINE);

    score = subwin(stdscr, 7, 4 * (virtualScale * 2) + 2,
                   box_next_y + 5 * virtualScale, box_next_x);
    box(score, ACS_VLINE, ACS_HLINE);

    wrefresh(win);
}

void start_tetris()
{
    int c, frame = 0;
    long int ms;
    time_t t;
    last_frame = clock();
    srand(time(NULL));
    init();
    draw_next_piece();
    draw_piece();
    draw_score();
    speed = get_speed();
    while (1)
    {
        t = clock();
        ms = (t - last_frame) * 1000 / CLOCKS_PER_SEC;

        if (input(getch()))
        {
            break;
        }

        if (ms > 50)
        {
            frame++;
            if (frame >= speed)
            {
                if (can_go_down())
                {
                    erase_piece();
                    move_down();
                    draw_piece();
                }
                else
                {
                    if (set_piece_tetris())
                    {
                        break;
                    }
                }
                frame = 0;
            }
            last_frame = clock();
        }
    }
}

void stop_tetris()
{
    endwin();
    delwin(win);
    delwin(next);
}

static int input(int code)
{
    int ret = 0;
    switch (code)
    {
    case 'q':
        ret = 1;
        break;

    case ' ':
        if (can_rotate())
        {
            erase_piece();
            rotate_piece();
            draw_piece();
        }
        break;

    case KEY_DOWN:
        if (can_go_down())
        {
            erase_piece();
            move_down();
            draw_piece();
        }
        else
        {
            if(set_piece_tetris())
            {
                ret = 1;
            }
        }
        break;

    case KEY_LEFT:
        if (can_go_left())
        {
            erase_piece();
            move_left();
            draw_piece();
        }
        break;

    case KEY_RIGHT:
        if (can_go_right())
        {
            erase_piece();
            move_right();
            draw_piece();
        }
        break;

    default:
        break;
    }
    return ret;
}

static void draw_case_absolute(int x, int y, color c, int x_vect, int y_vect)
{
    if (y >= 0)
    {
        x = 1 + x * (virtualScale * 2) + x_vect;
        y = 1 + y * virtualScale + y_vect;

        attron(COLOR_PAIR(c));
        for (int i = y; i < y + virtualScale; i++)
        {
            for (int j = x; j < x + virtualScale * 2; j++)
            {
                mvaddch(i, j, ' ');
            }
        }
        attroff(COLOR_PAIR(c));
    }
}

static void draw_case(int x, int y, color c)
{
    if (y >= 0)
    {
        draw_case_absolute(x, y, c, x_begin, y_begin);
    }
}

static void draw_piece()
{
    int x_p = get_x(),
        y_p = get_y(),
        tet = get_tetris(),
        rot = get_rotate();
    for (int y = 0; y < 4; y++)
    {
        for (int x = 0; x < 4; x++)
        {
            if (tetris[tet][rot][y][x])
            {
                draw_case(x + x_p, y + y_p, tet + 1);
            }
        }
    }
    refresh();
}

static void erase_piece()
{
    int x_p = get_x(),
        y_p = get_y(),
        tet = get_tetris(),
        rot = get_rotate();
    for (size_t y = 0; y < 4; y++)
    {
        for (size_t x = 0; x < 4; x++)
        {
            if (tetris[tet][rot][y][x])
            {
                draw_case(x + x_p, y + y_p, NONE);
            }
        }
    }
    refresh();
}

static int set_piece_tetris()
{
    int y_check[4] = {-1, -1, -1, -1};
    int line, line_count = 0;

    set_piece(y_check);

    for (size_t y = 0; y < 4; y++)
    {
        line = 1;
        for (size_t x = 0; x < 10; x++)
        {
            if (!get_case(x, y_check[y]))
            {
                line = 0;
            }
        }
        if (line)
        {
            erase_line(y_check[y]);
            line_count++;
        }
    }
    add_line(line_count);
    draw_score();
    speed = get_speed();

    update_tetris();
    if (loose())
    {
        return 1;
    }
    draw_piece();
    draw_next_piece();
    return 0;
}

static void erase_line(int y)
{
    for (size_t x = 0; x < 10; x++)
    {
        teleport_down_cols(x, y);
    }
}

static void teleport_down_cols(int x, int y)
{
    for (; y >= 0; y--)
    {
        set_case(get_case(x, y - 1), x, y);
        draw_case(x, y, get_case(x, y));
    }
}

static void draw_case_next(int x, int y, color c)
{
    draw_case_absolute(x, y, c, box_next_x, box_next_y);
}

static void draw_next_piece()
{
    int tet = get_next_tetris(),
        rot = 0;

    int y_box;
    switch (tet)
    {
    case 0:
    case 1:
    case 2:
    case 5:
    case 6:
        y_box = -1;
        break;

    case 3:
        y_box = 0;
        break;

    case 4:
        y_box = -2;

    default:
        break;
    }

    for (int y = 0; y < 4; y++)
    {
        for (int x = 0; x < 4; x++)
        {
            if (tetris[tet][rot][y][x])
            {
                draw_case_next(x, y + y_box, tet + 1);
            }
            else if (y + y_box >= 0 && y + y_box < 2)
            {
                draw_case_next(x, y + y_box, 0);
            }
        }
    }
    refresh();
}

static void draw_score()
{
    mvwprintw(score, 1, 1, "score :");
    mvwprintw(score, 2, 1, "%8d", get_score());
    mvwprintw(score, 4, 1, "level :", get_score());
    mvwprintw(score, 5, 1, "%8d", get_level());

    wrefresh(score);
}